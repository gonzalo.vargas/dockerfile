# Usa la imagen base de Ubuntu con Apache

FROM ubuntu

# Actualiza el sistema y instala herramientas necesarias

RUN apt update && apt install apache2 -y 

# Copia la configuración del sitio a la imagen

COPY ./mi-sitio.conf /etc/apache2/sites-available/000-default.conf

# Abre puertos del contenedor

EXPOSE 80 443

# Inicia el servidor Apache en primer plano

CMD ["apache2ctl", "-D", "FOREGROUND"]
