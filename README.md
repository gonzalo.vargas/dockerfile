# Trabajo Práctico Final  

 

Nombre y Apellido: Gonzalo Vargas

Materia: Infraestructura en la Nube



Instrucciones:

Crear un repositorio con las ramas 'main' y 'develop'.

La rama 'main' debe contener la version final del fichero 'index.hml' y el 'README.md'.

El README.md de la rama main debe contener las intrucciones genericas para:

\- Formatear y montar un volumen en un directorio a elección.
\- Lanzar un contenedor usando la imagen 'ubuntu/apache2' montando el directorio '/var/www/html' con el directorio asociado al volumen.
\- Clonar el repositorio y copiar el fichero 'index.html' de la rama 'main' al punto de montaje del contenedor.
\- Agregar un fichero de configuración de nginx para usar con proxy reverso.



  

### Paso 1: Crear un repositorio en GitLab

1. **Crear una cuenta en GitLab**:
   - Si no tienes una cuenta, regístrate en [GitLab](https://gitlab.com/users/sign_in).
2. **Crear un nuevo proyecto**:
   - Una vez iniciada la sesión, ve al menú de tu perfil y selecciona "New project".
   - Elige "Create blank project".
   - Asigna un nombre al proyecto, por ejemplo, `Dockerfile`.
   - Opcionalmente, agrega una descripción.
   - Selecciona la visibilidad del proyecto (público o privado).
   - Haz clic en "Create project".



### Paso 2:  Crear las Ramas `main` y `develop`



**Configurar el proyecto**:

- Clona el repositorio recién creado:

```
# git clone https://gitlab.com/gonzalo.vargas/dockerfile
# cd dockerfile
```

###### **Crear y cambiar a la rama `develop`**:

```
# git checkout -b develop
```

### Paso 3: Crear el Archivo `index.html` y `README.md` en la Rama `develop`



.**Crear el archivo `index.html`**:



```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Apache Server</title>
</head>
<body>
    <h1>Bienvenido al Servidor Apache</h1>
</body>
</html>
```



**Crear el archivo `README.md`**:



```
# Docker Apache Reverse Proxy
Este proyecto contiene una configuración básica para un servidor Apache en Docker.

## Requisitos
- Docker instalado en el sistema

## Instrucciones
- Formatear y montar un volumen en un directorio a elección.
- Lanzar un contenedor usando la imagen `ubuntu/apache2` montando el directorio `/var/www/html` con el directorio asociado al volumen.
- Clonar el repositorio y copiar el fichero `index.html` de la rama `main` al punto de montaje del contenedor.
- Agregar un fichero de configuración de nginx para usar con proxy reverso.


# Crear y montar un volumen
docker volume create volumen
docker run -d -p 8080:80 -v volumen:/var/www/html ubuntu/apache2

# Clonar el repositorio
git clone https://gitlab.com/gonzalo.vargas/dockerfile
cd dockerfile

# Copiar index.html al contenedor
docker cp index.html <container_id>:/var/www/html

# Configuración de proxy reverso con Nginx
server {
    listen 80;
    server_name yourdomain.com;

    location / {
        proxy_pass http://localhost:8080;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }
}
```



**Agregar y confirmar los archivos**:

```
# git add index.html README.md
# git commit -m "Agregar index.html y README.md a la rama develop"
# git push -u origin develop

```



### Paso 4: Crear y montar un volumen

```
# docker volume create volumen
```



### Paso 5: Lanzar un Contenedor con la Imagen `ubuntu/apache2`

```
# docker run -d -p 8080:80 -v volumen:/var/www/html ubuntu/apache2
```



### Paso 6: Clona el repositorio

```
# git clone https://gitlab.com/gonzalo.vargas/dockerfile
# cd dockerfile
```



### Paso 7: Copiar index.html al contenedor

```
docker cp index.html <container_id>:/var/www/html
```



### Paso 8: Fusionar `develop` en `main`



**Cambiar a la rama `main`**:

```
# git checkout main
```



**Fusionar los cambios de `develop`**:

```
# git merge develop
```



**Subir los cambios a la rama `main`**:

```
# git push origin main
```





### Paso 9: Configuración de Proxy Reverso con Nginx

Para configurar Nginx como un proxy reverso, crea un archivo de configuración `nginx-proxy.conf`:

```
server {
    listen 80;
    server_name yourdomain.com;

    location / {
        proxy_pass http://localhost:8080;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }
}
```





**Fuentes **

Enlaces a los sitios de los que se obtuvo información.

https://drive.google.com/drive/folders/1peuWym94nB97j2EbMexlNcL0_3tgSUcu

